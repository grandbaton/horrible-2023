using System;
using System.Collections.Generic;

namespace LudumDare50
{
    public class DialogueLine
    {
        public string _reference;
        public string _characterReference;
        public string _text;
        public readonly List<DialogueChoice> _choices = new List<DialogueChoice>();
    }
}