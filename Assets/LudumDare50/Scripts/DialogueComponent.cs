using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace LudumDare50
{
    public class DialogueComponent : MonoBehaviour
    {
        public string _dialogueReference;

        private void Start()
        {
            DialogueUiLayout dialogueUiLayout = GameObject.FindObjectOfType<DialogueUiLayout>();

            if (dialogueUiLayout != null)
            {
                Assert.IsTrue(dialogueUiLayout.HasDialog(_dialogueReference), $"{_dialogueReference} dialogue is missing!");
            }
        }
    }
}