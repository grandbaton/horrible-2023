using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace LudumDare50
{
    public class StartMenuManager : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        private void Update()
        {
            if (Mouse.current.leftButton.wasReleasedThisFrame)
            {
                SceneManager.LoadScene(1);
            }
        }
    }
}