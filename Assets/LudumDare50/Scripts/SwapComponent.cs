using System;
using UnityEngine;

namespace LudumDare50
{
    public class SwapComponent : MonoBehaviour
    {
        public InteractableComponent _elementToSwapWith;

        private void Start()
        {
            _elementToSwapWith.gameObject.SetActive(false);
        }
    }
}