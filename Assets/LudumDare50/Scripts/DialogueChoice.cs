using System.Collections.Generic;

namespace LudumDare50
{
    public class DialogueChoice
    {
        public class DialogElement
        {
            public KeywordType _keyword;
            public string _reference;
        }

        public string _text;
        public readonly List<DialogElement> _conditions = new List<DialogElement>();
        public readonly List<DialogElement> _options = new List<DialogElement>();
    }
}