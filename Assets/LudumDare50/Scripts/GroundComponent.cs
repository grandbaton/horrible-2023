using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

namespace LudumDare50
{
    public class GroundComponent : MonoBehaviour, IPointerClickHandler
    {
        private PlayerController _playerController;

        private void Start()
        {
            _playerController = GameObject.FindObjectOfType<PlayerController>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!_playerController.IsInteracting())
            {
                Vector3 targetPosition = eventData.pointerPressRaycast.worldPosition;

                Debug.DrawLine(targetPosition, targetPosition + Vector3.up * 5, Color.blue, 2);

                _playerController.SetDestination(targetPosition);
            }
        }
    }
}