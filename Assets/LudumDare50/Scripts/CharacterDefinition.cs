using UnityEngine;

namespace LudumDare50
{
    [CreateAssetMenu(fileName = "CharacterDefinition", menuName = "CharacterDefinition", order = 0)]
    public class CharacterDefinition : ScriptableObject
    {
        public string _characterReference;
        public Sprite _portrait;
    }
}