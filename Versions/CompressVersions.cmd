@echo off

for /f "delims=" %%a in ('wmic OS Get localdatetime ^| find "."') do set DateTime=%%a

set Yr=%DateTime:~0,4%
set Mon=%DateTime:~4,2%
set Day=%DateTime:~6,2%
set Hr=%DateTime:~8,2%
set Min=%DateTime:~10,2%
set Sec=%DateTime:~12,2%

set filename= %Yr%-%Mon%-%Day%[%Hr%-%Min%].LudumDare50

echo %filename%

C:\Utils\WinRAR\WinRar.exe a -ep1 -afzip -m5 -r %filename%.Windows.zip .\Windows\
C:\Utils\WinRAR\WinRar.exe a -ep1 -afzip -m5 -r %filename%.OSX.zip .\OSX\
C:\Utils\WinRAR\WinRar.exe a -ep1 -afzip -m5 -r %filename%.Linux.zip .\Linux\